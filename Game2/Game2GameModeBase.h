// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Game2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GAME2_API AGame2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
